﻿using System;
using System.IO;
using System.Threading;
using Amazon;
using Amazon.Polly;
using Amazon.Polly.Model;
using Amazon.Util;

namespace EpubToSsml
{
	public class Amazon : ISpeechSynthesisProvider
	{
		private AmazonPollyClient pc;

		public void Initialise()
		{
			pc = new AmazonPollyClient("redacted", "redacted", RegionEndpoint.EUWest1);
			AWSConfigs.AWSRegion = "eu-west-1";
			AWSConfigs.Logging = LoggingOptions.Console;
		}

		public void ProcessLineOfSsml(string line, string fileName)
		{
			SynthesizeSpeechResponse speech = GetSpeech(line);
			SaveSpeechToFile(fileName, speech);
		}

		private DateTime lastCall;

		private SynthesizeSpeechResponse GetSpeech(string inputText)
		{
			if (DateTime.Now - lastCall < TimeSpan.FromMilliseconds(50))
			{
				Thread.Sleep(50 - (DateTime.Now - lastCall).Milliseconds);
				lastCall = DateTime.Now;
			}

			SynthesizeSpeechRequest synthesizeSpeechRequest = new SynthesizeSpeechRequest
			{
				Text = inputText,
				OutputFormat = OutputFormat.Mp3,
				VoiceId = VoiceId.Joanna,
				TextType = TextType.Ssml
			};
			return pc.SynthesizeSpeechAsync(synthesizeSpeechRequest).Result;
		}

		private void SaveSpeechToFile(string filename, SynthesizeSpeechResponse response)
		{
			using (var fileStream = File.Create(filename))
			{
				response.AudioStream.CopyTo(fileStream);
				fileStream.Flush();
				fileStream.Close();
			}
		}
	}
}
