﻿using System;

namespace EpubToSsmlCore
{
	public interface ILog
	{
		void Log(string message, Severity severity = Severity.Info, Exception exception = null);
	}
}