﻿namespace EpubToSsmlCore
{
	public enum Severity
	{
		Debug,
		Info,
		Error
	}
}