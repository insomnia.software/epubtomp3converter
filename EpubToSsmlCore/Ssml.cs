﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using HtmlAgilityPack;

namespace EpubToSsml
{
	public static class Ssml
	{
		private static int longestPara = 0;
		public static int paragraphs = 0;
		private static long totalCharacterCount;

		public static string OutputFileName { get; private set; }

		/// <summary>
		/// This holds the entire ssml document being produced
		/// </summary>
		private static StringBuilder sb = new StringBuilder();

		/// <summary>
		/// Produces a file called nameOfBook.ssml
		/// </summary>
		/// <param name="chapters"></param>
		/// <param name="nameOfBook"></param>
		public static void GenerateOutputSsmlFile(List<Chapter> chapters, string nameOfBook)
		{
			ParseChapters(chapters);

			OutputFileName = $"{nameOfBook}.ssml";
			OutputSsml(OutputFileName);

			Console.WriteLine($"Book contains {totalCharacterCount} characters.");
			Console.WriteLine($"Book contains {paragraphs} paragraphs");
		}

		private static void ParseChapters(List<Chapter> chapters)
		{
			foreach (Chapter ch in chapters)
			{
				GenerateSsmlAndAddItToStringbuilder($"New chapter: {ch.Title}");
				ConvertHtmlAndAddItToStringBuilder(ch.Content);
			}
		}

		/// <summary>
		/// This doesn't return anything because it saves the output for manual editing and it's read back in later
		/// </summary>
		/// <param name="html"></param>
		private static void ConvertHtmlAndAddItToStringBuilder(string html)
		{
			HtmlDocument htmlDoc = new HtmlDocument();
			htmlDoc.LoadHtml(html);

			HtmlNode htmlBody = htmlDoc.DocumentNode.SelectSingleNode("//body");
			BuildSsml(htmlBody);
		}



		/// <summary>
		/// Take a HTML top level node and turn the html into sslm and add it to the stringbuilder
		/// </summary>
		/// <param name="node"></param>
		private static void BuildSsml(HtmlNode node)
		{
			if (node.Name == "p" || (node.NodeType == HtmlNodeType.Text && !string.IsNullOrWhiteSpace(node.InnerText)))
			{
				string whiteSpaceRemoved = node.InnerText.RemoveDuplicateWhiteSpace().Trim();
				string greaterLessThanSymbolsReplaced = whiteSpaceRemoved.Replace("&gt;", " greater than ").Replace("&lt;", " less than ").Replace("&amp;", " and ");
				//must do this after replacing < > since it replaces &lt; with <
				string unicodeConverted = System.Net.WebUtility.HtmlDecode(greaterLessThanSymbolsReplaced);
				
				GenerateSsmlAndAddItToStringbuilder(unicodeConverted);
				return;
			}

			foreach (HtmlNode childNode in node.ChildNodes)
				BuildSsml(childNode);
		}

		/// <summary>
		/// Add a line stripping white space and nulls
		/// </summary>
		/// <param name="s">String to append</param>
		static void GenerateSsmlAndAddItToStringbuilder(string s)
		{
			string ssml = GenerateSsml(s).SwapQuotesWithPause();
			if (string.IsNullOrWhiteSpace(ssml))
				return;
			sb.AppendLine(ssml);
		}

		/// <summary>
		/// Converts a single paragraph of html into valid ssml
		/// </summary>
		/// <param name="para">The paragraph to parse in plain text</param>
		/// <param name="comment">Optional comment to append to the start of the line. This can be used for tagging certain points</param>
		/// <returns>Valid SSML</returns>
		private static string GenerateSsml(string para, string comment = null)
		{
			if (string.IsNullOrWhiteSpace(para))
				return "";

			totalCharacterCount += para.Length;
			paragraphs++;

			if (para.Length > longestPara)
				longestPara = para.Length;

			if (para.Length > 1500)
				return SplitLargeParagraph(para);

			string ssmlString = "";
			XmlDocument doc = new XmlDocument();
			XmlElement element = (XmlElement)doc.AppendChild(doc.CreateElement("speak"));
			XmlNode node = element.AppendChild(doc.CreateElement("p"));
			node.InnerText = para;
			if (comment != null)
				ssmlString += $"<!--{comment}-->";
			ssmlString += doc.OuterXml;

			return ssmlString;
		}

		/// <summary>
		/// Split oversized paragraphs into ones within the limits of Amazon Poly.
		/// It splits on periods (senteces in most cases) and is no more intelligent.
		/// It will split at the mid point.
		/// Issues are splitting on "Sr." or other uses of periods, and not splitting on the 'best' sentences.
		/// </summary>
		/// <param name="para">Oversized paragraph</param>
		/// <returns>Ssml of paragraphs</returns>
		private static string SplitLargeParagraph(string para)
		{
			Console.WriteLine($"Splitting large paragraph of {para.Length} characters.");

			List<int> indexes = para.AllIndexesOf(".");

			int distanceOfClosestPeriodToMiddle = int.MaxValue;
			int indexOfClosest = -1;

			foreach (int i in indexes)
			{
				int distance = Math.Abs(para.Length / 2 - i);
				if (distance < distanceOfClosestPeriodToMiddle)
				{
					distanceOfClosestPeriodToMiddle = distance;
					indexOfClosest = i;
				}
			}

			if (indexOfClosest == -1)
				Debugger.Break();

			return GenerateSsml(para.Substring(0, indexOfClosest + 1)) + "\n" + GenerateSsml(para.Substring(indexOfClosest + 1).TrimStart());//trim the start because it'll have a space that was after the period in the last section
		}

		public static void OutputSsml(string filepath)
		{
			if(File.Exists(filepath))
			{
				Console.WriteLine("Output file exists, overwrite? Note: THIS WILL LOSE ALL CHANGES! Y/N");
				string s = Console.ReadLine();

				if (s != "Y" && s != "y")
					return;
			}


			TextWriter tw = new StreamWriter(filepath);
			tw.Write(sb);
			tw.Close();
		}
	}
}
