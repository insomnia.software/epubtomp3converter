﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpubToSsml
{
	/// <summary>
	/// Provides functionality to turn SSML based text into an audio file
	/// </summary>
	public interface ISpeechSynthesisProvider
	{
		/// <summary>
		/// Setup code such as API key registration
		/// </summary>
		void Initialise();
		
		/// <summary>
		/// Process the ssml, saving an mp3 file to disk
		/// </summary>
		/// <param name="line">The line to process</param>
		/// <param name="fileName">The file to save</param>
		void ProcessLineOfSsml(string line, string fileName);
	}
}
