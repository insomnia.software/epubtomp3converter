﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace EpubToSsmlCore
{
	public static class CrossPlatformHelper
	{
		/// <summary>
		/// For some reason OSX console input causes readline to fire twice, the result of the second is always empty string
		/// </summary>
		/// <returns>String entered by the user</returns>
		public static string ConsoleReadLine()
		{
			string input = Console.ReadLine();
			if(RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
				Console.ReadLine();//wtf mac
			return input;
		}
		
		/// <summary>
		/// Start a process on multiple platforms.
		/// This has only been tested on text files.
		/// See discussion here https://github.com/dotnet/corefx/issues/10361
		/// </summary>
		/// <param name="processFileName">The filename of the process to start. Or the filename of a file which will be opened with the default process assigned to that file type</param>
		/// <exception cref="PlatformNotSupportedException">Throws if platform is not windows, linux or OSX</exception>
		public static void ProcessStart(string processFileName)
		{
			if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
			{
				//Process.Start(processFileName);
			}
			else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
			{
				Process.Start("xdg-open", processFileName);
			}
			else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
			{
				Process.Start("open", processFileName);
			}
			else
			{
				throw new PlatformNotSupportedException("Unknown platform");
			}
		}
	}
}