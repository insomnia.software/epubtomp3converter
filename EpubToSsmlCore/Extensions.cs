﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace EpubToSsml
{
	public static class Extensions
	{
		public static string RemoveDuplicateWhiteSpace(this string para) => Regex.Replace(para, @"\s+", " ");

		public static string SwapQuotesWithPause(this string para)
		{
			para = para.Replace("\"", "<break strength=\"weak\"/>");
			para = para.Replace("”", "<break strength=\"weak\"/>");
			para = para.Replace("“", "<break strength=\"weak\"/>");

			return para;
		}

		/// <summary>
		/// Returns the index of every occurrence of a string within a larger string
		/// </summary>
		/// <param name="str">The large string to get the indexes of</param>
		/// <param name="value">The smaller substring to locate</param>
		/// <returns>Indexes of smaller string in larger string</returns>
		public static List<int> AllIndexesOf(this string str, string value)
		{
			List<int> indexes = new List<int>();
			for (int index = 0; ; index += value.Length)
			{
				index = str.IndexOf(value, index);
				if (index == -1)
					return indexes;
				indexes.Add(index);
			}
		}
	}
}