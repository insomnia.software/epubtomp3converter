﻿using System;
using System.IO;

namespace EpubToSsmlCore
{
	public sealed class ConsoleAndTextLog2 : ILog
	{
		private TextWriter TextWriter { get; }

		public ConsoleAndTextLog2(string logFileName = "Log.txt")
		{
			TextWriter = new StreamWriter(logFileName) {AutoFlush = true};
		}

		public void Log(string message, Severity severity = Severity.Info, Exception exception = null)
		{
			string logMessage = $"{severity}: {message}";

			if (exception != null)
				logMessage += $" Exception: {exception}";
			
			Console.WriteLine(logMessage);
			TextWriter.WriteLine(logMessage);
		}
	}
}