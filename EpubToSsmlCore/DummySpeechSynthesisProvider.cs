﻿using System.Threading;

namespace EpubToSsml
{
	/// <summary>
	/// Used for testing so as to not waste amazon credits
	/// </summary>
	public class DummySpeechSynthesisProvider : ISpeechSynthesisProvider
	{
		private const int TIME_TO_DO_ONE_REQUEST = 10;
		
		public void Initialise() {}

		/// <summary>
		/// Sleep for an estimation of time to complete a request
		/// </summary>
		/// <param name="line"></param>
		/// <param name="fileName"></param>
		public void ProcessLineOfSsml(string line, string fileName)
		{
			Thread.Sleep(TIME_TO_DO_ONE_REQUEST);
		}
	}
}