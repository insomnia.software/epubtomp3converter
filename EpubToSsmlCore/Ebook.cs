﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Amazon.Runtime.Internal;
using HtmlAgilityPack;
using VersOne.Epub;

namespace EpubToSsml
{
	public static class Ebook
	{
		public static List<Chapter> GetChaptersFromEbook(string bookLocation)
		{
			EpubBook e = EpubReader.ReadBook(bookLocation);
			List<Chapter> chapters = GetChapters(e);
			chapters = chapters.DistinctBy(x => x.Content).ToList();
			return chapters;
		}


		private static List<Chapter> GetChapters(EpubBook book)
		{
			List<Chapter> chapters = new List<Chapter>();
			foreach (EpubChapter epubChapter in book.Chapters)
			{
				chapters.AddRange(LolRecursion(epubChapter));
			}

			return chapters;
		}

		private static List<Chapter> LolRecursion(EpubChapter eChapter)
		{
			Chapter ch = new Chapter();
			List<Chapter> chapters = new List<Chapter>{ch};
			ch.Title = eChapter.Title;
			ch.Content = eChapter.HtmlContent;
			foreach (EpubChapter eSubChapter in eChapter.SubChapters)
				chapters.AddRange(LolRecursion(eSubChapter));

			return chapters;
		}
	}

	public class Chapter
	{
		public string Title { get; set; }
		public string Content { get; set; }
	}

	public static class Extentions
	{
		public static IEnumerable<TSource> DistinctBy<TSource, TKey>(
			this IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector)
		{
			var knownKeys = new HashSet<TKey>();
			return source.Where(element => knownKeys.Add(keySelector(element)));
		}
	}
}
