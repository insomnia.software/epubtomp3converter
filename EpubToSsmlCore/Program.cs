﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using EpubToSsml;
using Humanizer;
using Humanizer.Localisation;
using ShellProgressBar;
using Amazon = EpubToSsml.Amazon;

namespace EpubToSsmlCore
{
	/// <summary>
	/// Program to turn an .epub audio book into audio files using Amazon Poly or other providers
	/// Notable features:
	/// Interfaced providers, use Microsoft, Amazon and a dummy for testing.
	/// Saves progress and resumes because transactions are limited and/or cost money
	/// </summary>
	class Program
	{
		private const string PROGRESS_FILE_NAME = "progress";
		
		static void Main(string[] args)
		{
			if (args.Length == 0 || !IsArgumentValid(args[0]))
				Console.WriteLine("Must pass arguments. Valid ones are: -a (amazon -m (microsoft) -d (dummy)");

			FileInfo fileInfo = GetEpubFileNameFromUser();

			string fileNameNoExtension = Path.GetFileNameWithoutExtension(fileInfo.Name);

			string outputFileName = ProcessEbookToSsmlFile(fileInfo);

			GetUserToEditSsmlFile(outputFileName);

			string ssml = File.ReadAllText(outputFileName);
			Ssml.paragraphs = File.ReadAllLines(outputFileName).Length;

			if (!Directory.Exists(fileNameNoExtension))
				Directory.CreateDirectory(fileNameNoExtension);

			ISpeechSynthesisProvider speechSynthesisProvider = null;

			switch (args[0])
			{
				case "-a":
					speechSynthesisProvider = new EpubToSsml.Amazon();
					break;
				case "-m":
					speechSynthesisProvider = new EpubToSsml.Microsoft();
					break;
				case "-d":
					speechSynthesisProvider = new DummySpeechSynthesisProvider();
					break;
			}

			if (speechSynthesisProvider == null)
			{
				Log.Logging.Log($"Invalid {nameof(speechSynthesisProvider)} exiting.");
				return;
			}
			Log.Logging.Log($"Using provider: {speechSynthesisProvider.GetType()}");
			
			speechSynthesisProvider.Initialise();

			int startPosition = GetStartPosition(PROGRESS_FILE_NAME);

			GenerateSpeech(speechSynthesisProvider, $"{fileNameNoExtension}\\{fileNameNoExtension}", ssml, Ssml.paragraphs, startPosition);

			Console.WriteLine("Done");
			Console.ReadLine();
		}

		private static bool IsArgumentValid(string arg) => (arg == "-a" || arg == "-m" || arg == "-d");

		/// <summary>
		/// Check if the last run failed for some reason and if so, what position it failed at so we can resume from it
		/// </summary>
		/// <returns>Line to start at (0 for the beginning)</returns>
		private static int GetStartPosition(string progressFileName)
		{
			int startPosition = ReadStartPositionFromFile(progressFileName);

			if (startPosition == 0) return 0;
			
			bool? userAnswer = ConsoleHelper.AskUserYesOrNoQuestion($"Last run appears to have crashed at line {startPosition}. Continue from this position?");
			if (userAnswer.HasValue && userAnswer.Value)
				return startPosition;

			return 0;
		}

		/// <summary>
		/// Gets the position to start 
		/// </summary>
		/// <param name="progressFileName">Name of the file storing progress</param>
		/// <returns>Line to start at (0 if no crash)</returns>
		private static int ReadStartPositionFromFile(string progressFileName)
		{
			if (!File.Exists(progressFileName))
				return 0;
			
			int startPosition = 0;
			string last = null;
			try
			{
				last = File.ReadLines("progress").Last();
			}
			catch (Exception)
			{
			}

			if (!string.IsNullOrWhiteSpace(last))
				startPosition = int.Parse(last);
			return startPosition;
		}

		/// <summary>
		/// Ask the user to edit the output SSML file and wait for them to cinform that ahs been done
		/// </summary>
		/// <param name="outputFileName"></param>
		private static void GetUserToEditSsmlFile(string outputFileName)
		{
			Console.WriteLine($"Edit \"{outputFileName}\" as required. Press enter when done.");
			CrossPlatformHelper.ProcessStart(outputFileName);
			ConsoleHelper.GetUserInput("Press enter when done");
		}

		/// <summary>
		/// Turns an epub book into an ssml file
		/// </summary>
		/// <param name="fileInfo">File info for the ebook</param>
		/// <returns>Ssml output file name</returns>
		private static string ProcessEbookToSsmlFile(FileInfo fileInfo)
		{
			Log.Logging.Log("Extracting data from ebook");
			List<Chapter> chapters = Ebook.GetChaptersFromEbook(fileInfo.FullName);
			Log.Logging.Log("Extracted data, Generating SSML");

			Ssml.GenerateOutputSsmlFile(chapters, Path.GetFileNameWithoutExtension(fileInfo.Name));
			Log.Logging.Log("Generating SSML complete");
			return Ssml.OutputFileName;
		}

		/// <summary>
		/// Ask the user for the location of the epub book to process
		/// </summary>
		/// <returns>FileInfo of the epub book</returns>
		private static FileInfo GetEpubFileNameFromUser()
		{
			string fileName = ConsoleHelper.GetUserInput("Enter file path for epub ebook:");
			
			if (fileName.StartsWith("\"") && fileName.EndsWith("\""))//trim start and trailing quotes. This happens if using "copy as path" in windows
				fileName = fileName.Substring(1, fileName.Length - 2);
			
			FileInfo fileInfo = new FileInfo(fileName);

			if (!fileInfo.Exists)
			{
				Console.WriteLine($"Cannot find file {fileName}");
				return fileInfo;
			}

			Log.Logging = new ConsoleAndTextLog2($"{fileInfo.Name}.txt");
			return fileInfo;
		}

		/// <summary>
		/// Generate audio files from ssml file
		/// </summary>
		/// <param name="speechSynthesisProvider"></param>
		/// <param name="directoryToOutput">Directory to output audio files into</param>
		/// <param name="ssml">SSML to process</param>
		/// <param name="numberOfParagraphs">Only used to calculate a progress percentage</param>
		/// <param name="startPosition">Paragraph of ssml to start at if resuming from a crash</param>
		private static void GenerateSpeech(ISpeechSynthesisProvider speechSynthesisProvider, string directoryToOutput, string ssml, int numberOfParagraphs, int startPosition = 0)
		{
			using (StringReader reader = new StringReader(ssml))
			using (TextWriter progressWriter = new StreamWriter("progress") {AutoFlush = true})
			using (ProgressBar pbar = new ShellProgressBar.ProgressBar(numberOfParagraphs, "Calculating time remaining"))
			{
				try
				{
					string line;
					int paragraphsCompleted = 0;
					Stopwatch sw = new Stopwatch();
					sw.Start();
					do
					{
						line = reader.ReadLine();
						if (!string.IsNullOrWhiteSpace(line))
						{
							string nameOfAudioFile = numberOfParagraphs < 10000 ? $"{paragraphsCompleted++:D4}" : $"{paragraphsCompleted++:D5}";
							
							float etaInMs = 0;
							if (paragraphsCompleted != 0)
								etaInMs = sw.ElapsedMilliseconds / (float) paragraphsCompleted * (numberOfParagraphs - paragraphsCompleted);
							
							pbar.Tick($"{paragraphsCompleted}/{numberOfParagraphs} {TimeSpan.FromMilliseconds(etaInMs).Humanize(3, minUnit: TimeUnit.Second)} remaining");
							
							if (paragraphsCompleted >= startPosition)
								speechSynthesisProvider.ProcessLineOfSsml(line, $"{directoryToOutput}{nameOfAudioFile}.mp3");
							progressWriter.WriteLine(paragraphsCompleted);
						}
					} while (line != null);
				}
				catch (Exception exception)
				{
					progressWriter.Close();
					Log.Logging.Log($"Failed due to exception", Severity.Error, exception);
					return;
				}
			}

			File.Delete("progress");//cleanup so we don't think the end of this file was a crash for the next book 
		}
	}
}