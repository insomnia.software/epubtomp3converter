﻿using System;
using System.Runtime.InteropServices;
using NodaTime;
using NodaTime.Text;

namespace EpubToSsmlCore
{
	public static class ConsoleHelper
	{
		public static string GetUserInput(string question)
		{
			Console.WriteLine(question);
			return CrossPlatformHelper.ConsoleReadLine();
		}
		
		public static int GetUserIntegerInput(string question) => int.Parse(GetUserInput(question));

		public static LocalDate? GetUserDateInput(string question) => ParseLocalDateIso(GetUserInput(question));

		public static bool? AskUserYesOrNoQuestion(string question)
		{
			switch (GetUserInput(question).ToUpper())
			{
				case "Y":
				case "YES":
					return true;
				case "N":
				case "NO":
					return false;
				default:
					return null;
			}
		}
		
		public static LocalDate? ParseLocalDateIso(string input)
		{
			if (string.IsNullOrWhiteSpace(input))
				return null;
			LocalDatePattern localDatePattern = LocalDatePattern.CreateWithCurrentCulture("yyMMdd");
			ParseResult<LocalDate> parseResult = localDatePattern.Parse(input);

			if (parseResult.Success)
				return parseResult.Value;
			return null;
		}
	}
}