﻿using System;
using System.IO;
using System.Threading;
using CognitiveServicesTTS;

namespace EpubToSsml
{
	/// <summary>
	/// Never tested
	/// https://docs.microsoft.com/en-us/azure/cognitive-services/Speech/API-Reference-REST/BingVoiceOutput
	/// https://azure.microsoft.com/en-us/pricing/details/cognitive-services/speech-api/
	/// https://github.com/Azure-Samples/Cognitive-Speech-TTS/tree/master/Samples-Http/CSharp
	/// </summary>
	public class Microsoft : ISpeechSynthesisProvider
	{
		private Synthesize cortana;
		private string accessToken;
		private string fileName;

		public void Initialise()
		{
			Console.WriteLine("Starting Authtentication");

			// Note: The way to get api key:
			// Free: https://www.microsoft.com/cognitive-services/en-us/subscriptions?productId=/products/Bing.Speech.Preview
			// Paid: https://portal.azure.com/#create/Microsoft.CognitiveServices/apitype/Bing.Speech/pricingtier/S0
			Authentication auth = new Authentication("Your api key goes here");

			try
			{
				accessToken = auth.GetAccessToken();
				Console.WriteLine("Token: {0}\n", accessToken);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Failed authentication.");
				Console.WriteLine(ex.ToString());
				Console.WriteLine(ex.Message);
				return;
			}

			cortana = new Synthesize();

			cortana.OnAudioAvailable += PlayAudio;
			cortana.OnError += ErrorHandler;
		}

		public void ProcessLineOfSsml(string line, string fileName)
		{
			this.fileName = fileName;
			cortana.Speak(CancellationToken.None, new Synthesize.InputOptions()
			{
				RequestUri = new Uri("https://speech.platform.bing.com/synthesize"),
				// Text to be spoken.
				Text = line,
				VoiceType = Gender.Female,
				// Refer to the documentation for complete list of supported locales.
				Locale = "en-GB",
				// You can also customize the output voice. Refer to the documentation to view the different
				// voices that the TTS service can output.
				VoiceName = "Microsoft Server Speech Text to Speech Voice (en-GB, HazelRUS)",
				// Service can return audio in different output format.
				OutputFormat = AudioOutputFormat.Audio16Khz128KBitRateMonoMp3,
				AuthorizationToken = "Bearer " + accessToken,
			}).Wait();
		}

		/// <summary>
		/// This method is called once the audio returned from the service.
		/// It will then attempt to play that audio file.
		/// Note that the playback will fail if the output audio format is not pcm encoded.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="args">The <see cref="GenericEventArgs{Stream}"/> instance containing the event data.</param>
		private void PlayAudio(object sender, GenericEventArgs<Stream> args)
		{
			Console.WriteLine(args.EventData);

			using (var fileStream = File.Create(fileName))
			{
				args.EventData.CopyTo(fileStream);
				fileStream.Flush();
				fileStream.Close();
			}
			args.EventData.Dispose();
		}

		/// <summary>
		/// Handler an error when a TTS request failed.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="GenericEventArgs{Exception}"/> instance containing the event data.</param>
		private void ErrorHandler(object sender, GenericEventArgs<Exception> e)
		{
			Console.WriteLine("Unable to complete the TTS request: [{0}]", e.ToString());
		}
	}
}
